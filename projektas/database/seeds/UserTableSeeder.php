<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $role_klientas = Role::where('name', 'Klientas')->first();
    $role_admin  = Role::where('name', 'Admin')->first();
    $role_buhalteris  = Role::where('name', 'Buhalteris')->first();

    $klientas = new User();
    $klientas->name = 'Klientas';
    $klientas->email = 'klientas@pavyzdys.com';
    $klientas->password = bcrypt('secret');
    $klientas->save();
    $klientas->roles()->attach($role_klientas);

    $admin = new User();
    $admin->name = 'Admin';
    $admin->email = 'admin@pavyzdys.com';
    $admin->password = bcrypt('admin');
    $admin->save();
    $admin->roles()->attach($role_admin);

    $buhalteris = new User();
    $buhalteris->name = 'Buhalteris';
    $buhalteris->email = 'buhalteris@pavyzdys.com';
    $buhalteris->password = bcrypt('secret');
    $buhalteris->save();
    $buhalteris->roles()->attach($role_buhalteris);

    $klientas = new User();
    $klientas->name = 'test';
    $klientas->email = 'test@pavyzdys.com';
    $klientas->password = bcrypt('test');
    $klientas->save();
    $klientas->roles()->attach($role_klientas);
  }
}
