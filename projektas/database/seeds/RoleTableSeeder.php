<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
  {
    $role_admin = new Role();
    $role_admin->name = 'Admin';
    $role_admin->description = 'visagalis';
    $role_admin->save();

    $role_buhalteris = new Role();
    $role_buhalteris->name = 'Buhalteris';
    $role_buhalteris->description = 'mldc prie poperių';
    $role_buhalteris->save();

    $role_klientas = new Role();
    $role_klientas->name = 'Klientas';
    $role_klientas->description = 'visada teisus';
    $role_klientas->save();
  }
}
