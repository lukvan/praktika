<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout')->middleware('auth');
Route::get('klientas', 'LoginController@klientas')->name('klientas');
Route::get('admin', 'LoginController@admin')->name('admin');
Route::get('buhalteris', 'LoginController@buhalteris')->name('buhalteris');
Route::get('prisijunges', 'HomeController@prisijunges')->name('prisijunges');


// Route::get('klientas',function()
// {
// 	return view('klientas');
// });

// Route::get('admin',function()
// {
// 	return view('admin');
// });

// Route::get('buhalteris',function()
// {
// 	return view('buhalteris');
// });

// Route::get('/register',function()
// {
// 	return redirect('home');
// });