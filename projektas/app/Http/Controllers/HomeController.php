<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$request->user()->authorizeRoles(['Admin','Buhalteris','Klientas']);
        return view('home');
    }
    public function prisijunges()
    {
    if(Auth::user()->hasrole('Admin')){
        return view('admin');
    }
    elseif(Auth::user()->hasrole('Buhalteris'))
    {
        return view('buhalteris');
    }
    else
    {
        return view('klientas');
    }
}
     public function logout()
    {
      Auth::logout();
      return redirect('/');
    }
}
